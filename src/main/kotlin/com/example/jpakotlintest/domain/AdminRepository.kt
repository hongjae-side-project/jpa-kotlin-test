package com.example.jpakotlintest.domain

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface AdminRepository: CrudRepository<Admin, Int> {
    fun findByNickName(nickName: String): Admin?
}
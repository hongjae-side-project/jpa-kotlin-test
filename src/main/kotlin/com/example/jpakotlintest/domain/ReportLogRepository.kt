package com.example.jpakotlintest.domain

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ReportLogRepository: CrudRepository<ReportLog, Int> {

}
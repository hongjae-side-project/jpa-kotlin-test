package com.example.jpakotlintest.domain

import javax.persistence.*

@Entity
data class Admin(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int? = null,
    var nickName: String
    )

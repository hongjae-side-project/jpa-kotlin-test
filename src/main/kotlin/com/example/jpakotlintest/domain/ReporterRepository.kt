package com.example.jpakotlintest.domain

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ReporterRepository: CrudRepository <Reporter, Int> {
    fun findByNickName(nickName: String): Reporter?
}
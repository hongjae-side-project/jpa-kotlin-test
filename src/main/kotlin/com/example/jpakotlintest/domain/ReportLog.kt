package com.example.jpakotlintest.domain

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class ReportLog(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int? = null,
    var reporterId: Int,
    var reporterNickName: String?,
    //var adminId: Int,
    //var adminNickName: String
    )
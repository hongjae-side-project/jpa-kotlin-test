package com.example.jpakotlintest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class JpaKotlinTestApplication

fun main(args: Array<String>) {
    runApplication<JpaKotlinTestApplication>(*args)
}

package com.example.jpakotlintest

import com.example.jpakotlintest.domain.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@DataJpaTest(showSql = true)
class JpaKotlinTestApplicationTests {

    @Autowired lateinit var adminRepository: AdminRepository
    @Autowired lateinit var reporterRepository: ReporterRepository
    @Autowired lateinit var reportLogRepository: ReportLogRepository

    @BeforeEach
    fun setup() {
        println("-------------------- ADMIN --------------------")
        adminRepository.save(Admin(nickName = "SAM"))
        adminRepository.save(Admin(nickName = "KAISER"))
        adminRepository.save(Admin(nickName = "BEN"))


        println("-------------------- REPORTER --------------------")
        reporterRepository.save(Reporter(nickName = "KAISER"))
        reporterRepository.save(Reporter(nickName = "BEN"))
    }


    @Test
    fun findAdmin() {
        println("====================================================")
        println(adminRepository.findByNickName("SAM"))
        println(reporterRepository.findByNickName("KAISER"))
        println(reporterRepository.findByNickName("BEN"))
        println(adminRepository.findAll())
        println("====================================================")
    }

    @Test
    fun findReporter() {
        println("====================================================")
        println(reporterRepository.findByNickName("SAM"))
        println(reporterRepository.findByNickName("KAISER"))
        println(reporterRepository.findAll())
        println("====================================================")
    }

    @Test
    fun saveReportLog() {
        println("====================================================")
        val adminId:Int? = adminRepository.findByNickName("SAM")?.id
        val adminNickName:String? = adminRepository.findByNickName("SAM")?.nickName

        val reporterId:Int? = reporterRepository.findByNickName("KAISER")?.id
        val reporterNickName:String? = reporterRepository.findByNickName("KAISER")?.nickName

        val reporter2Id:Int? = reporterRepository.findByNickName("BEN")?.id
        val reporter2NickName:String? = reporterRepository.findByNickName("BEN")?.nickName

        println("adminId : "+adminId)
        println("adminNickName : "+adminNickName)

        println("reporterId : "+reporterId)
        println("reporterNickName : "+reporterNickName)

        println("reporter2Id : "+reporter2Id)
        println("reporter2NickName : "+reporter2NickName)

        println("====================== SAVE ======================")
        reporterRepository.save(Reporter(nickName = "BEN"))
        reportLogRepository.save(ReportLog(reporterId = reporterId!!, reporterNickName = reporterNickName))
        reportLogRepository.save(ReportLog(reporterId = reporterId!!, reporterNickName = reporterNickName))

        reportLogRepository.save(ReportLog(reporterId = reporter2Id!!, reporterNickName = reporter2NickName))
        reportLogRepository.save(ReportLog(reporterId = reporter2Id!!, reporterNickName = reporter2NickName))

        println("====================== REPORT LOG ======================")
        println(reportLogRepository.findAll())

    }
}
